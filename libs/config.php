<?php
$sitetitle  = "Link Protector";
$siteslogan = "Protecting your links!";
$scripturl  = "http://".$_SERVER['HTTP_HOST'].'/'; // URL ENDING WITH A SLASH

// Short URL

$shorturl = true; // Set true to enable short url feature e.g. when turned on, it will display yoursite.com/103MySites instead of yoursite.com/index.php?ID=103MySites

// Top 10 List
$topten = true; // Set true to enable the Top 10 Links, or false to disable the Top 10 Links

?>