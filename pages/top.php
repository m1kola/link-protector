<?php defined('SYSPATH') or die('No direct script access.'); ?>
<table class="table table-striped table-bordered">
<thead>
<tr>
<th class="text-center">#</th>
<th class="text-center">Name</th>
<th class="text-center">Views</th>
<th class="text-center">Date Added</th>
</tr>
</thead>
<tbody>
<?php
$short = !$shorturl ? 'index.php?ID=' : null;
if(!$topten) {
	die('<div class="alert alert-danger">Access denied!</div>');
}

$order = array();
$dirname = "./config";
$dh = opendir( $dirname ) or die("couldn't open directory");
while ($file = readdir($dh)) {
	if (!in_array($file, array('.','..','.DS_Store','.htaccess','index.html'))) {
		$fh = fopen ("./config/".$file, 'r');
		$list = explode('|', fgets($fh));
		$filecrc = str_replace(".dlp","",$file);
		$order[] = $list[4].','.$filecrc.','.$list[1].','.$list[4];
		fclose ($fh);
	}
}

if(count($order) > 0) { 
sort($order, SORT_NUMERIC);
$order = array_reverse($order);

$count = count($order) < 10 ? count($order) : 10;

for($i = 0; $i < $count; $i++) {
$line = explode(',', $order[$i]);
?>

<tr>
	<td class="text-center"><?php echo ($i + 1); ?></td>
	<td class="text-left"><a target="_blank" href="<?php echo $short .$filedata[0] . $line[1]; ?>"><?php echo $line[1]; ?></a></td>
	<td class="text-center"><?php echo $line[3]; ?></td>
	<td class="text-center"><?php echo date('d-m-Y H:i', $list[0]); ?></td>
</tr>

<?php }}else{ ?>
<tr><td colspan="4" class='text-center text-danger'>No data for display.</td></tr>
<?php } ?>

</tbody>
</table>