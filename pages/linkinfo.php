<?php defined('SYSPATH') or die('No direct script access.'); ?>
<?php
$fop   = fopen('./files/' . $_GET['ID'] . '.dlp', 'r');
$links = fread($fop, filesize('./files/' . $_GET['ID'] . '.dlp'));
fclose($fop);
$links   = explode("\n", $links);
?>
<div class="row">
	<div class="col-md-12 text-center">
		<h4>Hidden Links:</h4>
	</div>
	<div class="col-md-12 text-center">
	<?php foreach ($links as $url) { ?>
		<p><a target="_blank" href="<?php echo $url; ?>"><?php echo $url; ?></a></p>
	<?php } ?>

	<p>Views: <?php echo isset($views) ? $views : $content[4]; ?></p>
	</div>
</div>