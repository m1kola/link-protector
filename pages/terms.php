<?php defined('SYSPATH') or die('No direct script access.'); ?>
<div class="row">
	<div class="col-md-12 text-center">
		<h4>Terms and Conditions</h4>
	</div>
	<div class="col-md-12">
		<p><?php echo $sitetitle; ?> is a free service allowing any internet user to use any of our services freely, but must obey <?php echo $sitetitle; ?> terms and conditions. </p>
		<p><?php echo $sitetitle; ?> Terms of Service are subject to change without notice.</p>
		<p>All files are copyright to their respective owners. <?php echo $sitetitle; ?> directs full legal responsibility of files to their respective users. All other content copyright <?php echo $sitetitle; ?>. <?php echo $sitetitle; ?> is not responsible for the content any uploaded files, nor is it in affiliation with any entities that may be represented in the uploaded files.</p>
		<p>Except where diligently implied, absolutely NO part of <?php echo $sitetitle; ?> may be reproduced or recreated without explicit written permission by operators of <?php echo $sitetitle; ?> and certified written verification by Notary Public.
		</p>
		<p><?php echo $sitetitle; ?> also reserves the right to delete any of it's uploaded contents without reason and at any time.</p>
		<p>All information provided by the user is strictly confidential. <?php echo $sitetitle; ?> reserves the right to distribute aggregated demographic information provided by the user, but <?php echo $sitetitle; ?> will never release any personal information about the user without permission. However, <?php echo $sitetitle; ?> reserves the right to release user information if user has violated the <?php echo $sitetitle; ?> Terms of Service, if the user has committed unlawful acts, if the information is subpoenaed, or if <?php echo $sitetitle; ?> deems it necessary or appropriate.</p>
		<p>We use third-party advertising companies to serve ads when you visit our website. These companies may use information (not including your name, address, email address, or telephone number) about your visits to this and other websites in order to provide advertisements about goods and services of interest to you. This script is based on DaddyScripts, however DaddyScripts does not hold responsibility for how this, or other belonging scripts may have been used.</p>
		<p>Any media that you submit to <?php echo $sitetitle; ?> may be redistributed throughout the <?php echo $sitetitle; ?> network of sites, the internet, and other media channels, which may include third-party advertisers.</p>
	</div>
</div>