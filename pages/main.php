<?php defined('SYSPATH') or die('No direct script access.'); ?>
<p>Link Protector is a free service, that allows you to protect your link(s) and prevents any of your link(s) from being found/indexed from search engine spiders.</p>
<hr>
<form id="createLink">
	<div class="form-group">
		<label for="title">Title:</label>
		<input type="text" name="title" class="form-control" id="title">
	</div>
	<div class="form-group">
		<label for="links">Links:</label>
		<textarea style="resize: none;" name="information" id="links" class="form-control" rows="5"></textarea>
	</div>
	<div class="form-group password hide">
		<label for="passw">Password</label>
		<input type="password" name="passw" class="form-control" id="passw">
	</div>
	<div class="checkbox">
		<label>
			<input type="checkbox" class="checkboxStatus"> Enable Password Protect
		</label>
	</div>
	<span class="submit btn btn-primary">Create Protected Links!</span>
	<hr>
	<p class="small"><sup>*</sup> If a password isn't inputted, a captcha will be displayed before showing the 
links to prevent the links from being spidered.</p>
</form>