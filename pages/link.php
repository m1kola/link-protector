<?php defined('SYSPATH') or die('No direct script access.'); ?>
<?php
$_GET["ID"] = $_GET['page'];
$IDFile = "./files/" . $_GET["ID"] . ".dlp";

if (file_exists($IDFile)) {
	$fop     = fopen("./config/" . $_GET["ID"] . ".dlp", "r");
	$content = fread($fop, '999');
	fclose($fop);
	$content = explode("|", $content);

	$filePassword = $content[2];
	$userPassword = isset($_POST['passw']) && !empty($_POST['passw']) ? md5($_POST['passw']) : null;

	if($content[1] == 'Yes' && $filePassword != $userPassword) {
		require_once (__DIR__).'/password_req.php';
	}elseif($content[1] == 'No' && !isset($_SESSION['bypass'])){
		require_once (__DIR__).'/captchaSecurity.php';
	}else{
		if (!isset($_SESSION['views']) || $_SESSION['views'] != $_GET['ID']) {
			$fileconfig = fopen("./config/" . $_GET['ID'] . ".dlp", "w");
			fwrite($fileconfig, $content[0] . "|" . $content[1] . "|" . $content[2] . "|" . $content[3] . "|" . ($content[4] + 1));
			fclose($fileconfig);
			$_SESSION['views'] = $_GET['ID'];
			$views             = $content[4] + 1;
		}

		require_once (__DIR__).'/linkinfo.php';
	}
}
?>