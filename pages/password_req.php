<?php defined('SYSPATH') or die('No direct script access.'); ?>
<div class="row">
	<div class="col-md-12 text-center">
		<h4>Password Protected Link(s):</h4>
	</div>
	<div class="col-md-12">
		<form method="POST">
			<div class="form-group">
				<label for="passw">Password:</label>
				<div class="input-group">
					<input type="password" class="form-control" id="passw" name="passw">
					<span class="input-group-btn">
						<button type="submit" class="btn btn-primary">Submit</button>
					</span>
				</div>
			</div>
		</form>
	</div>
</div>