<?php defined('SYSPATH') or die('No direct script access.'); ?>
<div class="row">
	<div class="col-md-12 text-center">
		<h4>About Us</h4>
	</div>
	<div class="col-md-12">
		<p><?php echo $sitetitle; ?> is a free link protection program to avoid any links from being indexed by spiders and eventually being listed on search engines. Recently, it has been a known fact that some File Hosters have been using private spiders (bots) to track certain file names and delete them immediately without verification, even if the file never contained illegal contents just to avoid any law suits against them. Many users have reported to have special permission to upload and share some files, where the file hoster's spider would go and delete it anyway.</p>
		<p>Also, users reported their links being indexed by Google and other various search engines, some of the reasons being incorrect option settings or simply the spider didn't obey their options set by the uploader. <?php echo $sitetitle; ?>'s main goal is to hide these links from these automated bots where you are free to name the link's title to whatever suits you.</p>
	</div>
</div>