<?php defined('SYSPATH') or die('No direct script access.'); ?>
<?php
if(isset($_POST['code']) && $_SESSION['security_code'] == md5($_POST['code'])) {
	$_SESSION['bypass'] = md5(time());
}

?>

<div class="col-md-12 text-center">
	<h4>Captcha Check:</h4>
</div>
	
<div class="row">
	<div class="col-md-4 text-center col-md-offset-4">
		<img class="img-responsive img-thumbnail" src="CaptchaSecurityImages.php">
		<br><a href=""><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Reload</a>
		<br><br>
		<form method="POST">
		<div class="form-group">
			<input class="form-control" id="code" name="code" placeholder="Security Code">
		</div>
		<button type="submit" class="btn btn-primaty btn-block">Submit</button>
	</form>
	</div>
</div>

<hr>
<p class="small"><sup>*</sup> If the uploader has not chosen to password-protected their links, then we include a captcha check to avoid the links from being indexed by spiders.</p>