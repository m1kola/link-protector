<?php
if (session_status() !== PHP_SESSION_ACTIVE) {session_start();}

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);



define('SYSPATH', TRUE);

require_once (__DIR__).'/libs/config.php';

$pages = array('main.html','top.html','terms.html','aboutus.html');
$page = isset($_GET['page']) && !empty($_GET['page']) ? $_GET['page'] : 'main.html';

require_once (__DIR__).'/template/header.php';
if(file_exists((__DIR__).'/pages/'.str_replace('.html','.php',$page))) {
	require_once (__DIR__).'/pages/'.str_replace('.html','.php',$page);
}else{
	require_once (__DIR__).'/pages/link.php';
}
require_once (__DIR__).'/template/footer.php';
?>