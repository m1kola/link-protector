<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo $sitetitle; ?> | <?php echo $siteslogan; ?></title>
	<link rel="stylesheet" type="text/css" href="public/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="public/css/sweetalert.css">
</head>
<body>
<div style="height: 70px;" class="hidden-xs hidden-sm"></div>
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<nav style="border-radius: 4px 4px 0 0;" class="navbar navbar-inverse">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#"><?php echo $sitetitle; ?></a>
					</div>

					<div class="collapse navbar-collapse" id="menu">
						<ul class="nav navbar-nav navbar-right">
							<li><a href="main.html">Home</a></li>
							<?php if($topten): ?>
							<li><a href="top.html">Top 10 Links</a></li>
							<?php endif; ?>
							<li><a href="terms.html">Terms of Use & Privacy</a></li>
							<li><a href="aboutus.html">About Us</a></li>
						</ul>
					</div>
				</div>
			</nav>
			<div style="position: relative; top: -20px; border-radius: 0 0 4px 4px; border-top: none;" class="panel panel-default">
				<div class="panel-body">